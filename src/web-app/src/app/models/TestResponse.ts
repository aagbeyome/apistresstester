export interface ITestResponse {
    requestId: string;
    responseId: string;
    timeElapsed: number;
    status: number;
}

export class TestResponse implements ITestResponse {

    public requestId: string;
    public responseId: string;
    public timeElapsed: number;
    public status: number;

    constructor(requestId: string, responseId: string, timeElapsed: number, status: number) {
        this.requestId = requestId;
        this.responseId = responseId;
        this.timeElapsed = timeElapsed;
        this.status = status;
    }
};