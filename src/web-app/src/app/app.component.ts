import { Component, OnInit } from '@angular/core';
import { SocketService } from './services/socket.service';
import { TestResponse } from './models/TestResponse';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title: string = 'web-app';

  //test stats
  requestCount: number = 1000;
  responseCount: number = 0;
  avgResponseTime: number = 0;
  totalElapsedTime = 0;
  isTestRunning = false;
  totalSuccess = 0;
  totalFailures = 0;


  testResponseSubscription = null;
  currentRequestId = "";

  constructor(private socketService: SocketService) {
    this.testResponseSubscription = this.socketService.getSocket().fromEvent<TestResponse>('testResponse');
  }

  /**
   * @name AppComponent.ngOnInit
   * @methodOf AppComponent
   * @description
   * Lifecycle event
   * @param (void)
   * @returns (void)
   */
  ngOnInit() {
    this.createSubscription()
  }

  reset() {
    this.responseCount = 0;
    this.avgResponseTime = 0;
    this.totalElapsedTime = 0;
    this.totalSuccess = 0;
    this.totalFailures = 0;
  }

  stopSubcription() {
    this.isTestRunning = false
    this.testResponseSubscription.unsubscribe();
    this.testResponseSubscription = null
  }

  createSubscription() {
    if (!this.testResponseSubscription) {
      this.stopSubcription()
    }

    this.testResponseSubscription.subscribe((testResponse) => {
      this.processResponse(testResponse)
    });

  }

  onAbortTest() {
    this.isTestRunning = false
    this.socketService.abortStressTests(this.currentRequestId)
  }

  getSuccessRate() {
    let avg = Math.round((this.totalSuccess / this.requestCount) * 100)
    return avg
  }

  getSuccessPercent() {
    return `${this.getSuccessRate()}%`
  }

  processResponse(testResponse: TestResponse) {

    this.responseCount += 1
    this.totalElapsedTime += testResponse.timeElapsed
    this.avgResponseTime = parseFloat((this.totalElapsedTime / this.responseCount).toFixed(1))

    if (testResponse.status == 200) {
      this.totalSuccess += 1
    } else {
      this.totalFailures += 1
    }

    if (this.responseCount >= this.requestCount) {
      this.isTestRunning = false
    }
  }

  getRandomId(): string {
    return Math.random().toString(36).substr(2, 10).toUpperCase();
  }

  /**
   * @name AppComponent.onStartTests
   * @methodOf AppComponent
   * @description
   * Button click event handler to send request to server
   * @param (void)
   * @returns (void)
   */
  onStartTests() {
    if (this.requestCount > 0) {
      this.isTestRunning = true
      this.reset()
      this.currentRequestId = this.getRandomId()
      this.socketService.startStressTests(this.requestCount, this.currentRequestId)
    }
  }




  /**
   * @name AppComponent.ngOnDestroy
   * @methodOf AppComponent
   * @description
   * cancel subscription to receive messages from websocket
   * @param (void)
   * @returns (void)
   */
  ngOnDestroy() {
    this.stopSubcription()
  }

}
