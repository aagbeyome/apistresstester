import { TestResponse } from '../entities/TestResponse';
const axios = require('axios').default;

export class LoadTester {
    private static abortedRequestIds: Set<string> = new Set()
    private static backendUrl = "http://localhost:7002/mockService";

    static runRequests(reqCount: number, reqId: string, socket: any) {

        const getRandomId = (): string => {
            return Math.random().toString(36).substr(2, 10).toUpperCase();
        };

        const done = (tElapsed: any, response: any, socket: any, reqId: string) => {
            if (!this.abortedRequestIds.has(reqId)) {
                tElapsed = process.hrtime(tElapsed);
                let tElapse = (parseFloat(`${tElapsed[0]}.${tElapsed[1]}`)).toFixed(3)
                let { status } = response
                if (!status) { status = 400 }
                let res = new TestResponse(reqId, getRandomId(), parseFloat(tElapse), status)
                socket.emit("testResponse", res)
            }

        }

        const sendRequest = async (socket: any, reqId: string) => {
            let timeElapsed = process.hrtime();
            try {
                const response = await axios.get(this.backendUrl);

                done(timeElapsed, response, socket, reqId)
            } catch (error) {
                done(timeElapsed, error, socket, reqId)
            }
        }

        for (let i = 0; i < reqCount; i++) {
            if (!this.abortedRequestIds.has(reqId)) {
                sendRequest(socket, reqId)
            }
        }

    }

    static abortRequest(reqId: string) {
        this.abortedRequestIds.add(reqId)
    }

}