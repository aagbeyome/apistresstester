import './LoadEnv'; // Must be the first import
import app from '@server';
import logger from '@shared/Logger';
import { LoadTester } from './services/LoadTester';

enum SocketOption {
    abort = "ABORT",
    run = "RUN"
}

//socket io
const server = require("http").Server(app);
let io = require("socket.io")(server);

// Start the server
const port = Number(process.env.PORT || 3000);
server.listen(port, () => {
    logger.info('Express server started on port: ' + port);
});

//socket
io.on("connection", function (socket: any) {
    console.log(`Client ${socket.id} connected`);

    socket.on(SocketOption.run, (payload: any) => {
        console.info('payload from server', payload);
        const { requestCt, requestId } = payload
        LoadTester.runRequests(requestCt, requestId, socket)
    });

    socket.on(SocketOption.abort, (requestId: string) => {
        LoadTester.abortRequest(requestId)
    })

});


